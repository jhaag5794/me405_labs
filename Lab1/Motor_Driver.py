# -*- coding: utf-8 -*-

''' @file Motor_Driver.py
This file contains the class MotorDriver with all the commands for driving the motor
and a test programm for executing some of those commands.

    
@author Jens Haag
@date 05/14/20'''




class MotorDriver:
    '''Motor driver object
    
    This class implements the motor driver for the Nucleo Board.
    @author Jens Haag
    @date 04/24/20'''
    
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer_number,timer_freq):
        '''Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        
        @param EN_pin A string object of the enable pin. e.g. ''B6''
        @param IN1_pin A string object to use as the input to half bridge 1. e.g ''B4''
        @param IN2_pin A string object to use as the input to half bridge 2. e.g ''B5''
        @param timer_number An integer of the Timer_Channel_Number to use for PWM generation on IN1_pin
        and IN2_pin.
        @param timer_freq An integer of the Timer used as the Switching_Frequenz of the PWM'''
        
        # Init the Motor Driver Pins
        self.EN_pin = pyb.Pin (EN_pin, pyb.Pin.OUT_PP)
        self.IN1_pin = pyb.Pin (IN1_pin,pyb.Pin.OUT_PP)
        self.IN2_pin = pyb.Pin (IN2_pin,pyb.Pin.OUT_PP)
        self.tim = pyb.Timer(timer_number,freq = timer_freq)
        
        self.tcha_2 = self.tim.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
        self.tcha_1 = self.tim.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
        
        # Set Motor of for safety
        self.EN_pin.low()
        
        # Set both PMW to 0
        self.tcha_1.pulse_width_percent(0)
        self.tcha_2.pulse_width_percent(0)
        
        print ('Creating a motor driver')


    def enable (self):
        '''Enable the spinning of the Motor.
        This function will pull the EN_pin to high'''
        
        # Set Motor enable
        self.EN_pin.high()
        print ('Enabling Motor')

    def disable (self):
        '''Disable the spinning of the Motor.
        This function will pull the EN_pin to low'''
        
        # Set Motor disable
        self.EN_pin.low()
        print ('Disabling Motor')

    def set_duty (self, duty):            
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
        
        # Duty >= 0 --> clockwise ; Duty < 0 --> anti.clockwise
        if (duty>=0):
            self.tcha_1.pulse_width_percent(0)
            self.tcha_2.pulse_width_percent(abs(duty))
        else:
            self.tcha_1.pulse_width_percent(abs(duty))
            self.tcha_2.pulse_width_percent(0) 


if __name__ == '__main__':
    
    # Import Libary
    import pyb
    
    # Create the pin objects used for interfacing with the motor driver
    pin_EN = 'A10'
    pin_IN1 = 'B4'
    pin_IN2 = 'B5'

    # Create the timer object used for PWM generation
    timer_number = 3
    timer_freq = 20000

    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, timer_number,timer_freq)
    
    # Enable the motor driver
    moe.enable()
    
    # Set the duty cycle to 10 percent; spinning clockwise
    moe.set_duty(10)
    
    # Create Var for Loop Control
    idx='0';
    
    while idx != '4':
        
        idx=input('Enable Motor (1), Disable Motor (2), Set Duty_Cicle (3), End Programm (4) ')
        
        if (idx== '1'): 
            # Enable the motor driver
            moe.enable();
            
        elif (idx=='2'): 
            # Disable the motor driver
            moe.disable();
            
        elif (idx=='3'): 
            
            # Create Var for Loop Control
            contr_1=1;
            
            # Check valid duty_cycle input
            while contr_1:
                try:
                    duty_cyl = int(input('Please enter the duty cycle in [%]: '))       
                except ValueError:
                    print("Not an integer! Try again.")
                    continue
                else:
                    if ((duty_cyl>100) or duty_cyl <(-100)):
                        print("Duty Cycle between -100 and 100! Try again")
                    else:
                        contr_1=0
                        
            # Set the duty cycle to duty_cyl        
            moe.set_duty(duty_cyl)
            
        elif (idx=='4'): 
            # Disable the motor driver before ending (safety)
            moe.disable();
            print("End Programm")
        else:
                print("Please enter an valid index! Try again.")