# -*- coding: utf-8 -*-

''' @file Motor_Encoder.py
This file contains the class Encoder with all the commands for geting the motor position
and a test programm for executing some of those commands.

    
@author Jens Haag
@date 05/14/20'''


class Encoder:
    '''Encoder object
    
    This class implements the motor encoder for the Nucleo Board.
    @author Jens Haag
    @date 05/06/20'''
    
    #Counts Index for Number of Motor
    count_init=0
    

    def __init__ (self, ENA_pin, ENB_pin,timer_number,timer_prescaler,timer_period):
        '''Creates a motor encoder by initializing GPIO
        pins and initialize necessary variables for object.
        
        @param ENA_pin A string object of the first input pin. e.g ''B6''
        @param ENB_pin A string object of the second input pin. e.g ''B7''
        @param timer_number An integer of the Timer_Channel_Number
        @param timer_prescaler An integer of the Timer_Prescaler_Number
        @param timer_period An integer of the Timer_Period_Number'''
        
        
        # Init the Motor Driver Pins
        self.ENA_pin = pyb.Pin(ENA_pin, pyb.Pin.IN)
        self.ENB_pin = pyb.Pin(ENB_pin, pyb.Pin.IN)
        self.tim = pyb.Timer(timer_number,prescaler=timer_prescaler,period=timer_period)
        self.pin_A=self.tim.channel(1,self.tim.ENC_A, pin=self.ENA_pin)
        self.pin_B=self.tim.channel(2,self.tim.ENC_B, pin=self.ENB_pin)
        
        # Init variables for Encoder_functions
        self.timer_period=timer_period #stores timer_period
        self.index=Encoder.count_init #stores actual Motor_Index 
        self.motor_position=0 #stores act. Position of Motor
        
        
        # Set Counter of Timer to a known value
        self.set_position(10)
        
        # Init Variable for delta_func; stores two last values 
        self.posi_update = [10,10]
        
        
        # Helpful Output for Init; contains important information and Motor_Index
        print ('Init Encoder[{:}] ; Pins= {:}&{:} ; {:}'.format(Encoder.count_init,ENA_pin,ENB_pin,self.tim))
    
        #Increase Motor_Indexnumber for next call
        Encoder.count_init=Encoder.count_init+1
    
        

    def get_position (self):
        ''' This function reads the actual position of the timer.'''
        
        # reads actual timer and returs it
        return self.tim.counter()
    
    def set_position (self,target_position):
        ''' This function sets the position of the timer to specific value.
        Checks the Input and stores the new position of the timer
        
        @param target_position An integer holding the target timerposition'''
        
        #Checks Inputvalue
        if (target_position>self.timer_period) or (target_position<0):         
            print ('Check Input value for target position! Valid values: 0 - {:}'.format(self.timer_period))
        else:
            #set timer to target_position
            self.tim.counter(target_position)
        
    def update(self):
        ''' This function stores the actual position of the timer and sets the 
        previous value to second latest position '''
        
        #sets actual latest position value to second latest position value
        self.posi_update[1]=self.posi_update[0]
        
        #stores actual position of timer to latest position value of motor
        self.posi_update[0]=self.get_position()
        
    def get_delta(self):
        ''' This function calculates the delta of ticks between two update()_calls.
        If a Timer_Overflow has occurred in the meantime, the delta_value gets corrected.
        Finally the delta_value is added to the Motor_Position_value'''
        
        #calculate delta between two latest update()_calls
        self.delta=self.posi_update[0]-self.posi_update[1]

        #Checks Timer_Overflow and recalculate delta_value
        if (self.delta > (self.timer_period/2) ):
            self.delta=self.delta-self.timer_period-1
        elif (self.delta<(-self.timer_period/2)):
            self.delta=self.delta+self.timer_period+1   
            
        #Adds delta_value to Motor_Position_value
        self.motor_position=self.motor_position+self.delta
        
    def print_updates(self):
        ''' This function gets the latest Motorinformation and print them'''
        
        #Gets latest Timer_Information
        self.update()
        
        #Calculate new delta and Motor_Position
        self.get_delta()
        
        # Output for User with Motorindex; delta_ticks and Motor_Position in ticks
        print ('Position Motor[{:}]: {:} Ticks, latest delta_ticks: {:}'.format(self.index,self.motor_position,self.delta))
        
    def get_updates(self):
        ''' This function gets the latest Motorinformation and 
        return the latest position'''
        
        #Gets latest Timer_Information
        self.update()
        
        #Calculate new delta and Motor_Position
        self.get_delta()
        
        return self.motor_position
    
    def reset_position(self):
        'This function resets the Motorposition to 0'
        
        #Set Motor_Position to zero -> Reset
        self.motor_position=0 
        

if __name__ == '__main__':
    ''' The main function creates different Encoder_Object with userspecific Inputvalues.
    All Encoder_Objects gets stored in an Array.
    The While_Loop prints the latest Motor_Encoder_'''
    
    # Import Libarys
    import pyb
    import time 
    
    # Init empty Array for Encoder_Objects
    enco=[]
    
    '''---Userinput---: 
        copy this statement: 
        enco.append(Encoder( First Inputpin , Second Inputpin , Timer_Number , Timer_Prescaler , Timer_Period )) 
        and add your values. e.g. 'enco.append(Encoder('B6','B7',4,0,0xFFFF))'
        First Inputpin: A string object of the first input pin. e.g ''B6''
        Second Inputpin: A string object of the second input pin. e.g ''B7''
        Timer_number: An integer of the Timer_Channel_Number
        Timer_Prescaler: An integer of the Timer_Prescaler_Number
        Timer_Period: An integer of the Timer_Period_Number
        
        Copy the statment for each Encoder_Object, you want to add.'''
        
    # Creates first Encoder
    enco.append(Encoder('B6','B7',4,0,0xFFFF))
    
    # Creates second Encoder
    enco.append(Encoder('C6','C7',8,0,0xFFFF))
    
    print(' ')
    
    #Prints latest Positioninformation of every Motor every 5 seconds
    while 1:
        for act_enco in enco:
            act_enco.print_updates()
        
        print(' ')
        time.sleep(5)



    
    
