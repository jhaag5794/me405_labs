
'''
@file       controller.py
@brief      PID_Controller
@details    This file contains the PID_controller for the balacing robot
            
@page       page_PID_Controler PID_Controler

@brief      PID_Controler Documentation

@details

@section    sec_PID_Controler_Task Task
            This function runs the PID-Controller on the board.

@section    sec_PID_Controler_imp Implementation
            This task is implemented by the PID_controller class. See controller.py
'''


class PID_controller:
    
    '''
    @brief      PID_Controller
    @details    This class implements the controller.
    '''
    
    def __init__ (self,angle_offset,kp_gain,ki_gain,kd_gain):
        '''
        @brief      Constructor for PID_Controller
        @details    This constructor is where the initialization code for the 
                    PID_Controller runs.
   
        @param angle_offset An integer object of the balaced angle 
        @param kp_gain A float object of the Kp_Value of the controller
        @param encoder_class Contains the Encoder of the systems
        @param driver_class Contains the Driver_object of the systems
        '''
        
        ## @brief   stabil angle for balacing
        #  @details Contains the stabil angle for balacing the robot     
        #
        self.angle_offset=angle_offset
        
        ## @brief   kp_Value of PID_Controller
        #  @details Contains the kp_Value of PID_Controller      
        #
        self.kp_gain=kp_gain
        
        ## @brief   ki_Value of PID_Controller
        #  @details Contains the ki_Value of PID_Controller      
        #
        self.ki_gain=ki_gain
        
        ## @brief   kd_Value of PID_Controller
        #  @details Contains the kd_Value of PID_Controller      
        #
        self.kd_gain=kd_gain
        
        ## @brief   Integral of angle difference for I-Controller
        #  @details Contains the intergral of angle differences for PID_Controller      
        #
        self.error_Integral=0
        
        
    def update(self,euler_angle,angle_vel):
        '''Controller Unit of the object.
        Gets latest Position of the Encoder and calculates the duty_cycle for controll'''
        
        
        # Calculates the Error-Signal
        err_angle=euler_angle-self.angle_offset
        
        # Calculates the Duty_Cycle
        act_sig=err_angle*self.kp_gain+self.error_Integral*self.ki_gain+angle_vel*self.kd_gain
        
        self.error_Integral=self.error_Integral+err_angle
        
        if self.error_Integral>10:
            self.error_Integral=10
        
        
        # Checks if Duty_Cycle is out of Range
        if abs(act_sig)>100:
            if act_sig>100:
                act_sig=100
            else:
                act_sig=-100    
        
        # Returns calculated duty_cycle
        return act_sig