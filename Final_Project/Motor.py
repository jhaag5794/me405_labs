'''
@file       Motor.py
@brief      MotorDriver
@details    This file contains the Motor Driver
            
@page       page_Motor_Driver Motor_Driver

@brief      Motor_Driver Documentation

@details

@section    sec_Motor_Driver_Task Task
            This function runs the motors of the robot

@section    sec_Motor_Driver_imp Implementation
            This task is implemented by the Robot_MotorDriver class.
'''


class Robot_MotorDriver:
    '''Motor driver object
    
    This class implements the motor driver for the Nucleo Board.
    '''
    
    
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer_number,timer_freq):
        '''Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        
        @param EN_pin A string object of the enable pin. e.g. ''B6''
        @param IN1_pin A string object to use as the input to half bridge 1. e.g ''B4''
        @param IN2_pin A string object to use as the input to half bridge 2. e.g ''B5''
        @param timer_number An integer of the Timer_Channel_Number to use for PWM generation on IN1_pin
        and IN2_pin.
        @param timer_freq An integer of the Timer used as the Switching_Frequenz of the PWM'''
        
        import pyb
        
        ## @brief   Enable/Disable pin of the Motor
        self.EN_pin = pyb.Pin (EN_pin, pyb.Pin.OUT_PP)
        ## @brief   First Pin for PWM of Motor
        self.IN1_pin = pyb.Pin (IN1_pin,pyb.Pin.OUT_PP)
        ## @brief   Second Pin for PWM of Motor
        self.IN2_pin = pyb.Pin (IN2_pin,pyb.Pin.OUT_PP)
        ## @brief   Timer for the PMW
        self.tim = pyb.Timer(timer_number,freq = timer_freq)
        
        

        
        ## @brief   PWM_Generation for the first Pin
        #  @details Contains the PWM for the first Pin of the Motor      
        #
        self.tcha_1 = self.tim.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
        
        ## @brief   PWM_Generation for the second Pin
        #  @details Contains the PWM for the second Pin of the Motor      
        #
        self.tcha_2 = self.tim.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
        
        # Set Motor of for safety
        self.EN_pin.low()
        
        # Set both PMW to 0
        self.tcha_1.pulse_width_percent(0)
        self.tcha_2.pulse_width_percent(0)
        
        print ('Creating a motor driver')


    def enable (self):
        '''Enable the spinning of the Motor.
        This function will pull the EN_pin to high'''
        
        # Set Motor enable
        self.EN_pin.high()
        print ('Enabling Motor')

    def disable (self):
        '''Disable the spinning of the Motor.
        This function will pull the EN_pin to low'''
        
        # Set Motor disable
        self.EN_pin.low()
        print ('Disabling Motor')

    def set_duty (self, duty):            
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
        
        # Duty >= 0 --> clockwise ; Duty < 0 --> anti.clockwise
        if (duty>=0):
            self.tcha_1.pulse_width_percent(0)
            self.tcha_2.pulse_width_percent(abs(duty))
        else:
            self.tcha_1.pulse_width_percent(abs(duty))
            self.tcha_2.pulse_width_percent(0)