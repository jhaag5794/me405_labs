
'''
@file       main.py
@brief      Initializing all parameters and runs the balancing loop

@details    The main script initializes all parameters needed to execute the 
            balancing loop. This includes setting up the I2C communication with \n 
            the IMU, setting the parameters for the PID controller and setting up 
            the motor control.
            This loop balances the robot. First the pitch angle and the pitch angle 
            velocity are read out of the IMU. Then these variables are send \n
            to the PID controller. The PID controller calculates a duty_cycle for
            the motors. \n\n
            The duty_cycle value must exceed a threshold value before it is sent to the motors. 
            The reason for this is that with small duty_cycles, sometimes only one motor \n
            rotates because they have different friction.\n
            If the value falls below the threshold a high-frequency square-wave signal is 
            sent to the motors to keep them moving. This prevents the motors from entering \n
            static friction as this is often greater than sliding friction.

@page       page_Main Main script

@details    Main script file that runs continuously.

@section    sec_Main_script_intro Introduction
            The main script file initializing the classes and then running
            the balancing loop at a fixed interval.\n
            

@section    sec_Main_script_init Initializing
            The main script initializes all parameters needed to execute the 
            balancing loop. This includes setting up the I2C communication with \n 
            the IMU, setting the parameters for the PID controller and setting up 
            the motor control.

@section    sec_Main_script_Balancing Balancing loop
            This loop balances the robot. First the pitch angle and the pitch angle 
            velocity are read out of the IMU. Then these variables are send \n
            to the PID controller. The PID controller calculates a duty_cycle for
            the motors. \n\n
            The duty_cycle value must exceed a threshold value before it is sent to the motors. 
            The reason for this is that with small duty_cycles, sometimes only one motor \n
            rotates because they have different friction.\n
            If the value falls below the threshold a high-frequency square-wave signal is 
            sent to the motors to keep them moving. This prevents the motors from entering \n
            static friction as this is often greater than sliding friction.
            
@section    sec_Main_script_Detail Detail Code Information
            For detailed code information see main.py
            \n\n
        
@author Jens Haag
@date 04/24/20
'''
    
# Import Libarys
import utime
import pyb

from IMU import IMU_communication 
from controller import PID_controller
from Motor import Robot_MotorDriver


## @brief   Adress_ID of IMU device
#  @details A Python hex that represents the adress_ID of the IMU-Unit 
#  
BNO055_I2C_ADDRESS = 0x28   

## @brief   Operationmode of the IMU
#  @details A Python string that represents the Operationmode of the IMU.
#           Possible Operationsmodes are: 'ACC','MAG','GYR','ACCMAG','ACCGYRO','MAGGYRO',
#           'AMG','IMUPLUS','COMPASS','M4G','NDOF_FMC_OFF','NDOF'
#  
Operation_Mode='NDOF'

## @brief   Angle at which the robot is balanced in degree
#  @details A Python integer that represents the stabil angle of the robot 
# 
angle_offset=0

## @brief   kp_coeffiecent of PID_Controller in [percent/degree]
#  @details A Python integer that represents the kp_value for the PID_controller 
# 
kp_gain=6 

## @brief   ki_coeffiecent of PID_Controller in [percent/degree]
#  @details A Python integer that represents the ki_value for the PID_controller 
# 
ki_gain=0.5


## @brief   kd_coeffiecent of PID_Controller in [percent*seconds/degree]
#  @details A Python integer that represents the kd_value for the PID_controller 
# 
kd_gain=0.3


## @brief   Threshold value for duty cycle in %
#  @details A Python integer that represents the minimal value of the duty cycle
#           which will be send to the motors
#  
duty_threshold=20

## @brief   Task loop rate in uS
#  @details A Python integer that represents the nominal number of microseconds
#           between iterations of the task loop.
#  
interval = 10000

## @brief   Status of square-wave signal 
#  @details A Python integer that represents the status for the square-wave signal.
#           Changes between 0 and 1
#  
wave_status=1

## @brief   Pin for status of Sensor Calibration 
#  @details The LED on the Nucleo shows the status of Sensor Calibration
#  
sensor_calb = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

## @brief   Set Status of Sensor  Calibration to false ()
sensor_calb.low ()



## @brief   Init Motor_Driver for Motor 1
#  @details Use Pin A10 for enable/disable and Timer 3 (B4 and B5) for PWM
# 
moe_1 = Robot_MotorDriver('A10', 'B4', 'B5', 3,20000)  

## @brief   Init Motor_Driver for Motor 2
#  @details Use Pin C1 for enable/disable and Timer 5 (A0 and A1) for PWM
# 
moe_2 = Robot_MotorDriver('C1', 'A0', 'A1', 5,20000)

## @brief   Init IMU Communication
#  @details Init the I2C-Bus and sets IMU to defined operation mode
# 
imc=IMU_communication(BNO055_I2C_ADDRESS,Operation_Mode)

## @brief   Init PID_controller
#  @details Init the by setting angle_offset and all gain values
# 
PID_con= PID_controller(angle_offset,kp_gain,ki_gain,kd_gain)


## @brief   Get calibration status of sensor
#  @details Get calibration status of sensor (check before putting robot on the ground)
#
cal_stat=imc.get_calibration_status()

print('Calibration Status: \nSystem({:}), Gyro({:}), Acc({:}), Mag({:})'.format(cal_stat[0],cal_stat[1],cal_stat[2],cal_stat[3]))


# Enable the motor driver
moe_1.enable()
moe_2.enable()

# Start Balacing
print ('Start')


## @brief   The timestamp of the next iteration of the task loop
#  @details A value from utime.ticks_us() specifying the timestamp for the next
#           iteration of the task loop.
#
#           This value is updated every time the task loop runs to schedule the
#           next iteration of the task loop.
#
next_time = utime.ticks_add(utime.ticks_us(), interval)


## @brief   The timestamp associated with the current iteration of the task
#           loop
#  @details A value from utime.ticks_us() updated every time the task loop 
#           checks to see if it needs to run. As soon as this timestamp exceeds
#           next_time the task loop runs an iteration.
#
cur_time = utime.ticks_us()       
        
while 1:
    # Update the current timestamp
    cur_time = utime.ticks_us()

    # Check if its time to run the tasks yet
    if utime.ticks_diff(cur_time, next_time) > 0:
    
    # If it is time to run, update the timestamp for the next iteration and
    # then run each task in the task list. 
        next_time = utime.ticks_add(next_time, interval)
                                    
        # Get calibration status of sensor and toggle LED 
        cal_stat=imc.get_calibration_status()
        # ACC, Gyro and Mag full calibratetd?
        if (cal_stat[1]==3 and cal_stat[2]==3 and cal_stat[3]==3):
            sensor_calb.high ()
        else:
            sensor_calb.low ()
    
        ## @brief   Get euler angles from IMU
        euler_angles=imc.read_euler()
        
        ## @brief   Get angular velocities from IMU
        ang_vel=imc.read_gyroscope()
        
        ## @brief   Calculate the Duty_Cycle with the use of a PID-Controller
        act_sig=PID_con.update(euler_angles[1],ang_vel[0])
        
        
        # Due to friction, small duty_cycle values can only cause just one motor to turn
        # If the value is too small, the motors are permanently rotated by the high-frequency square-wave signal. 
        # Thus problems with the control from static friction to sliding friction can be avoided
        if abs(act_sig)<duty_threshold:
            if (wave_status==1):
                
                moe_1.set_duty(-25)
                moe_2.set_duty(-25)
                
                wave_status=0
            else:
                moe_1.set_duty(25)
                moe_2.set_duty(25)
                
                wave_status=1
        else:
            #Motors get spinned with calculated value
            moe_1.set_duty(act_sig)
            moe_2.set_duty(act_sig)
        
    
    

