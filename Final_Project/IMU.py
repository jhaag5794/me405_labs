'''
@file       IMU.py
@brief      IMU_Communication
@details    This file contains the communication with the IMU_unit
            
@page       page_IMU_com IMU_Communication

@brief      IMU_Communication Documentation

@details

@section    sec_IMU_com_Task Task
            This function runs the communication between the board and the IMU.

@section    sec_IMU_com_imp Implementation
            This task is implemented by the IMU_communication. See IMU.py  
'''


class IMU_communication:
    '''IMU object
    
    This class implements the Communication with the BOSCH Sensor.

    Possible Operationsmodes are: 'ACC','MAG','GYR','ACCMAG','ACCGYRO','MAGGYRO',
                                 'AMG','IMUPLUS','COMPASS','M4G','NDOF_FMC_OFF',
                                 'NDOF'.
    For details about the modes check the IMU data sheet (3.3 Operation Modes)
    '''
    
    def __init__ (self,I2C_ADDRESS,CONF_Mode=None):
        '''Creates a IMU function by initializing the IMU_driver 
        The IMU_class is writen for DFRobot Board with Bosch BNO055 IMU.
        
        @param I2C_ADDRESS The I2C adresse of the IMU
        @param CONF_Mode A string object of Operation Mode of the IMU'''
        
        from pyb import I2C 
        import time
        
        # Get Register Adresses von Init_Register 
        self.Init_Register()
        
        ## @brief I2C-Adress of Device  
        self.BNO055_I2C_ADDRESS=I2C_ADDRESS
        
        ## @brief Stabile angle of Robot [rad]
        self.alpha=0*3.14159/180;
        
        ## @brief I2C Communication bus
        self.i2c = I2C(1, I2C.MASTER)
        
        # Set IMU to config mode
        self._config_mode()
    
        # Set PWR_MODE to NORMAL
        self.i2c.mem_write(self.POWER_MODE_NORMAL, self.BNO055_I2C_ADDRESS, self.BNO055_PWR_MODE_ADDR)
        
        # Match AXIS_CONF with breakout board
        self.i2c.mem_write(0x21, self.BNO055_I2C_ADDRESS, self.BNO055_AXIS_MAP_CONFIG_ADDR)
        self.i2c.mem_write(0x00, self.BNO055_I2C_ADDRESS, self.BNO055_AXIS_MAP_SIGN_ADDR)
        
        # Stores selected Mode and set IMU to this mode
        self._mode= None
        self._update_conf_mode(CONF_Mode)
        
        # Saeftytime to to make sure every thing works 
        time.sleep(1)
    
    def Init_Register(self):
        '''Contains all register_adresses for the IMU 
        By storing all adresses in different variables, is it easy to read and use the code'''
        
        ## @brief Operation Mode register
        self.BNO055_OPR_MODE_ADDR = 0X3D
        ## @brief Power Mode register
        self.BNO055_PWR_MODE_ADDR = 0X3E
        
        
        ## @brief Calibration status register
        self.BNO055_CALIB_STAT_ADDR               = 0X35

        
        ## @brief Value for Power Mode Normal
        self.POWER_MODE_NORMAL = 0X00
        ## @brief Value for Power Mode Low
        self.POWER_MODE_LOWPOWER = 0X01
        ## @brief Value for Power Mode Suspend
        self.POWER_MODE_SUSPEND = 0X02
        
        # Operation mode settings
        ## @brief Value for Operation Mode Configuration
        self.OPERATION_MODE_CONFIG                = 0X00
        ## @brief Value for Operation Mode Acc only
        self.OPERATION_MODE_ACCONLY               = 0X01
        ## @brief Value for Operation Mode Mag only
        self.OPERATION_MODE_MAGONLY               = 0X02
        ## @brief Value for Operation Mode Gyro only
        self.OPERATION_MODE_GYRONLY               = 0X03
        ## @brief Value for Operation Mode Acc & Mag
        self.OPERATION_MODE_ACCMAG                = 0X04
        ## @brief Value for Operation Mode Acc & Gyro
        self.OPERATION_MODE_ACCGYRO               = 0X05
        ## @brief Value for Operation Mode Mag & Gyro
        self.OPERATION_MODE_MAGGYRO               = 0X06
        ## @brief Value for Operation Mode Acc & Gyro & Mag
        self.OPERATION_MODE_AMG                   = 0X07
        ## @brief Value for Operation Mode IMU_Plus
        self.OPERATION_MODE_IMUPLUS               = 0X08
        ## @brief Value for Operation Mode Compass
        self.OPERATION_MODE_COMPASS               = 0X09
        ## @brief Value for Operation Mode M4G
        self.OPERATION_MODE_M4G                   = 0X0A
        ## @brief Value for Operation Mode 9 DOF (Fast Calibr off)
        self.OPERATION_MODE_NDOF_FMC_OFF          = 0X0B
        ## @brief Value for Operation Mode 9 DOF (Fast Calibr on)
        self.OPERATION_MODE_NDOF                  = 0X0C
        
        
        # Axis remap registers
        ## @brief Axis Configuartion register
        self.BNO055_AXIS_MAP_CONFIG_ADDR          = 0X41
        ## @brief Axis sign register
        self.BNO055_AXIS_MAP_SIGN_ADDR            = 0X42
        
        ## @brief Offset registers
        self.OFFSET_ADDR       = 0X55
        
        ## @brief Euler data register
        self.BNO055_EULER_ADDR        = 0X1A
        
        ## @brief Accel data register
        self.BNO055_ACCEL_DATA_ADDR   = 0X08
        
        ## @brief Mag data register
        self.BNO055_MAG_DATA_ADDR     = 0X0E
        
        ## @brief Gyro data register
        self.BNO055_GYRO_DATA_ADDR    = 0X14
        
        ## @brief Linear acceleration data register
        self.BNO055_LINEAR_ACCEL_DATA_ADDR  = 0X28
        
        ## @brief Gravity data register
        self.BNO055_GRAVITY_DATA_ADDR       = 0X2E
        
    
    def set_mode(self, mode):
        '''Set operation mode for BNO055 sensor
        This function sets the IMU to a new operation mode
        
        @param mode The number of the target Operaion Mode'''
        
        self.i2c.mem_write(mode, self.BNO055_I2C_ADDRESS, self.BNO055_OPR_MODE_ADDR)
        
        # Delay for 30 milliseconds (datsheet recommends 19ms, but a little more
        # can't hurt)
        time.sleep(0.03)
        
    def _config_mode(self):
        '''Sets the IMU to conf_mode
        This function sets the IMU to config_mode. Necessary to update settings of IMU'''
        
        # Enter configuration mode.
        self.set_mode(self.OPERATION_MODE_CONFIG)
        
    def _operation_mode(self):
        '''Sets the IMU to stored operation mode
        This function sets the IMU to stored operation_mode. Necessary after config_mode'''
        
        # Enter operation mode to read sensor data
        self.set_mode(self._mode)
        
    def _update_conf_mode(self,mode):
        '''Updates the stored operation_mode of IMU and set new mode
        This function updates the stored operation_mode and set the IMU to this mode
        
        @param mode A string object of Operation Mode of the IMU'''
        
        
        # Translate string in register_adress
        if (mode=='ACC'):
            self._mode=self.OPERATION_MODE_ACCONLY
        elif (mode=='MAG'):
            self._mode=self.OPERATION_MODE_MAGONLY
        elif (mode=='GYR'):
            self._mode=self.OPERATION_MODE_GYRONLY
        elif (mode=='ACCMAG'):
            self._mode=self.OPERATION_MODE_ACCMAG        
        elif (mode=='ACCGYRO'):
            self._mode=self.OPERATION_MODE_ACCGYRO
        elif (mode=='MAGGYRO'):
            self._mode=self.OPERATION_MODE_MAGGYRO            
        elif (mode=='AMG'):
            self._mode=self.OPERATION_MODE_AMG
        elif (mode=='IMUPLUS'):
            self._mode=self.OPERATION_MODE_IMUPLUS
        elif (mode=='COMPASS'):
            self._mode=self.OPERATION_MODE_COMPASS
        elif (mode=='M4G'):
            self._mode=self.OPERATION_MODE_M4G
        elif (mode=='NDOF_FMC_OFF'):
            self._mode=self.OPERATION_MODE_NDOF_FMC_OFF
        elif (mode=='NDOF'):
            self._mode=self.OPERATION_MODE_NDOF
        else:
            print('Invalid Modus selected')
            if self._mode==None:
                self._mode=self.OPERATION_MODE_NDOF
                print('Modus was set to NDOF')
        
        # Set IMU to new mode
        self._operation_mode()
    
    def conv_to_sign_int(self,Bytes_string):
        '''Converts a 2 Byte string object to sign int
        This function converts a 2 Byte string object to an sign int value
        
        @param Bytes_string A string object of 2 Byte-Array'''
        
        # Convert string object to unsign int
        sint_number=int.from_bytes(Bytes_string, 'little')
        
        # Conv unsign int to sign int
        if (sint_number>=((0xFFFF+1)/2)):
            sint_number=sint_number-0xFFFF-1
            
        return sint_number
    
    def get_calibration_status(self):
        '''Read the calibration status of the sensors and return a 4 tuple with
        calibration status as follows:
          - System, 3=fully calibrated, 0=not calibrated
          - Gyroscope, 3=fully calibrated, 0=not calibrated
          - Accelerometer, 3=fully calibrated, 0=not calibrated
          - Magnetometer, 3=fully calibrated, 0=not calibrated
        '''
        
        # Return the calibration status register value.
        data = self.i2c.mem_read(1,self.BNO055_I2C_ADDRESS,self.BNO055_CALIB_STAT_ADDR)
        
        # Conv Byte string to unsign int
        cal_status=int.from_bytes(data,'little')

        # Gets 2 significant bits for status of object
        sys = (cal_status >> 6) & 0x03
        gyro = (cal_status >> 4) & 0x03
        accel = (cal_status >> 2) & 0x03
        mag = cal_status & 0x03
        
        # Return the results as a tuple of all 4 values.
        return (sys, gyro, accel, mag)
    
    def _read_vector(self, address, count=3):
        '''Read count number of 16-bit signed values starting from the provided
        address. Returns a tuple of the values that were read.
        
        @param adresse The first register_adresse to start reading values
        @param count  An interger of the number of 16-bit values, that should be read'''
        
        # Init array with size of count
        result = [0]*count        
        
        # Read a sign 16-bit value
        for i in range(count):
            data = self.i2c.mem_read(2, self.BNO055_I2C_ADDRESS, address+i*2)
            result[i]=self.conv_to_sign_int(data)
        
        # Return values as array
        return result
    
    def read_euler(self):       
        '''Return the current absolute orientation as a tuple of yaw, roll,
        and pitch euler angles in degrees.
        '''
        # Read Euler-Angles by starting with first Euler_Angle_Register
        yaw, roll, pitch = self._read_vector(self.BNO055_EULER_ADDR)
        
        return (yaw/16.0, roll/16.0, pitch/16.0)
    
    def read_magnetometer(self):
        '''Return the current magnetometer reading as a tuple of X, Y, Z values
        in micro-Teslas.'''
        
        x, y, z = self._read_vector(self.BNO055_MAG_DATA_ADDR)
        return (x/16.0, y/16.0, z/16.0)

    def read_gyroscope(self):
        '''Return the current gyroscope (angular velocity) reading as a tuple of
        X, Y, Z values in degrees per second.'''
        
        x, y, z = self._read_vector(self.BNO055_GYRO_DATA_ADDR)
        
        return (x/900.0, y/900.0, z/900.0)

    def read_accelerometer(self):
        '''Return the current accelerometer reading as a tuple of X, Y, Z values
        in meters/second^2.'''
        
        x, y, z = self._read_vector(self.BNO055_ACCEL_DATA_ADDR)
        return (x/100.0, y/100.0, z/100.0)

    def read_linear_acceleration(self):
        '''Return the current linear acceleration (acceleration from movement,
        not from gravity) reading as a tuple of X, Y, Z values in meters/second^2.'''
    
        x, y, z = self._read_vector(self.BNO055_LINEAR_ACCEL_DATA_ADDR)
        return (x/100.0, y/100.0, z/100.0)

    def read_gravity(self):
        '''Return the current gravity acceleration reading as a tuple of X, Y, Z
        values in meters/second^2.'''

        x, y, z = self._read_vector(self.BNO055_GRAVITY_DATA_ADDR)
        return (x/100.0, y/100.0, z/100.0)
