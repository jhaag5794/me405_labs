# -*- coding: utf-8 -*-
"""
Created on Wed May 20 14:59:48 2020

@author: jenshaag
"""


''' @file IMU_Driver.py
This file contains the class IMU_driver with all the commands for geting the motor position
and a test programm for executing some of those commands.
    
@author Jens Haag
@date 05/20/20'''

class IMU_driver:
    '''IMU object
    
    This class implements the Communication with the BOSCH Sensor.

    Possible Operationsmodes are: 'ACC','MAG','GYR','ACCMAG','ACCGYRO','MAGGYRO',
                                 'AMG','IMUPLUS','COMPASS','M4G','NDOF_FMC_OFF',
                                 'NDOF'.
    For details about the modes check the IMU data sheet (3.3 Operation Modes)
    
    @author Jens Haag
    @date 05/20/20'''
    
    def __init__ (self,I2C_ADDRESS,CONF_Mode=None):
        '''Creates a IMU function by initializing the IMU_driver 
        The IMU_class is writen for DFRobot Board with Bosch BNO055 IMU.
        
        @param I2C_ADDRESS The I2C adresse of the IMU
        @param CONF_Mode A string object of Operation Mode of the IMU'''
        
        # Get Register Adresses von Init_Register 
        self.Init_Register()
        
        # Store Adress of Device
        self.BNO055_I2C_ADDRESS=I2C_ADDRESS
        
        # Create and init as a master
        self.i2c = I2C(1, I2C.MASTER)
        
        # Set IMU to config mode
        self._config_mode()
    
        # Set PWR_MODE to NORMAL
        self.i2c.mem_write(self.POWER_MODE_NORMAL, self.BNO055_I2C_ADDRESS, self.BNO055_PWR_MODE_ADDR)
        
        # Match AXIS_CONF with breakout board
        self.i2c.mem_write(0x24, self.BNO055_I2C_ADDRESS, self.BNO055_AXIS_MAP_CONFIG_ADDR)
        self.i2c.mem_write(0x00, self.BNO055_I2C_ADDRESS, self.BNO055_AXIS_MAP_SIGN_ADDR)
        
        # Stores selected Mode and set IMU to this mode
        self._mode= None
        self._update_conf_mode(CONF_Mode)
        
        # Saeftytime to to make sure every thing works 
        time.sleep(1)
    
    def Init_Register(self):
        '''Contains all register_adresses for the IMU 
        By storing all adresses in different variables, is it easy to read and use the code'''
        
        # Mode registers
        self.BNO055_OPR_MODE_ADDR = 0X3D
        self.BNO055_PWR_MODE_ADDR = 0X3E
        
        
        # Status registers
        self.BNO055_CALIB_STAT_ADDR               = 0X35
        self.BNO055_SELFTEST_RESULT_ADDR          = 0X36
        self.BNO055_INTR_STAT_ADDR                = 0X37
        
        # Power modes
        self.POWER_MODE_NORMAL = 0X00
        self.POWER_MODE_LOWPOWER = 0X01
        self.POWER_MODE_SUSPEND = 0X02
        
        # Operation mode settings
        self.OPERATION_MODE_CONFIG                = 0X00
        self.OPERATION_MODE_ACCONLY               = 0X01
        self.OPERATION_MODE_MAGONLY               = 0X02
        self.OPERATION_MODE_GYRONLY               = 0X03
        self.OPERATION_MODE_ACCMAG                = 0X04
        self.OPERATION_MODE_ACCGYRO               = 0X05
        self.OPERATION_MODE_MAGGYRO               = 0X06
        self.OPERATION_MODE_AMG                   = 0X07
        self.OPERATION_MODE_IMUPLUS               = 0X08
        self.OPERATION_MODE_COMPASS               = 0X09
        self.OPERATION_MODE_M4G                   = 0X0A
        self.OPERATION_MODE_NDOF_FMC_OFF          = 0X0B
        self.OPERATION_MODE_NDOF                  = 0X0C
        
        
        # Axis remap registers
        self.BNO055_AXIS_MAP_CONFIG_ADDR          = 0X41
        self.BNO055_AXIS_MAP_SIGN_ADDR            = 0X42
        
        # Offset registers
        self.OFFSET_ADDR       = 0X55
        
        # Euler data register
        self.BNO055_EULER_ADDR        = 0X1A
        
        # Accel data register
        self.BNO055_ACCEL_DATA_ADDR   = 0X08
        
        # Mag data register
        self.BNO055_MAG_DATA_ADDR     = 0X0E
        
        # Gyro data register
        self.BNO055_GYRO_DATA_ADDR    = 0X14
        
        # Linear acceleration data register
        self.BNO055_LINEAR_ACCEL_DATA_ADDR  = 0X28
        
        # Gravity data register
        self.BNO055_GRAVITY_DATA_ADDR       = 0X2E
        
    
    def set_mode(self, mode):
        '''Set operation mode for BNO055 sensor
        This function sets the IMU to a new operation mode
        
        @param mode The number of the target Operaion Mode'''
        
        self.i2c.mem_write(mode, self.BNO055_I2C_ADDRESS, self.BNO055_OPR_MODE_ADDR)
        
        # Delay for 30 milliseconds (datsheet recommends 19ms, but a little more
        # can't hurt)
        time.sleep(0.03)
        
    def _config_mode(self):
        '''Sets the IMU to conf_mode
        This function sets the IMU to config_mode. Necessary to update settings of IMU'''
        
        # Enter configuration mode.
        self.set_mode(self.OPERATION_MODE_CONFIG)
        
    def _operation_mode(self):
        '''Sets the IMU to stored operation mode
        This function sets the IMU to stored operation_mode. Necessary after config_mode'''
        
        # Enter operation mode to read sensor data
        self.set_mode(self._mode)
        
    def _update_conf_mode(self,mode):
        '''Updates the stored operation_mode of IMU and set new mode
        This function updates the stored operation_mode and set the IMU to this mode
        
        @param mode A string object of Operation Mode of the IMU'''
        
        
        # Translate string in register_adress
        if (mode=='ACC'):
            self._mode=self.OPERATION_MODE_ACCONLY
        elif (mode=='MAG'):
            self._mode=self.OPERATION_MODE_MAGONLY
        elif (mode=='GYR'):
            self._mode=self.OPERATION_MODE_GYRONLY
        elif (mode=='ACCMAG'):
            self._mode=self.OPERATION_MODE_ACCMAG        
        elif (mode=='ACCGYRO'):
            self._mode=self.OPERATION_MODE_ACCGYRO
        elif (mode=='MAGGYRO'):
            self._mode=self.OPERATION_MODE_MAGGYRO            
        elif (mode=='AMG'):
            self._mode=self.OPERATION_MODE_AMG
        elif (mode=='IMUPLUS'):
            self._mode=self.OPERATION_MODE_IMUPLUS
        elif (mode=='COMPASS'):
            self._mode=self.OPERATION_MODE_COMPASS
        elif (mode=='M4G'):
            self._mode=self.OPERATION_MODE_M4G
        elif (mode=='NDOF_FMC_OFF'):
            self._mode=self.OPERATION_MODE_NDOF_FMC_OFF
        elif (mode=='NDOF'):
            self._mode=self.OPERATION_MODE_NDOF
        else:
            print('Invalid Modus selected')
            if self._mode==None:
                self._mode=self.OPERATION_MODE_NDOF
                print('Modus was set to NDOF')
        
        # Set IMU to new mode
        self._operation_mode()
    
    def conv_to_sign_int(self,Bytes_string):
        '''Converts a 2 Byte string object to sign int
        This function converts a 2 Byte string object to an sign int value
        
        @param Bytes_string A string object of 2 Byte-Array'''
        
        # Convert string object to unsign int
        sint_number=int.from_bytes(Bytes_string, 'little')
        
        # Conv unsign int to sign int
        if (sint_number>=((0xFFFF+1)/2)):
            sint_number=sint_number-0xFFFF-1
            
        return sint_number
    
    def get_calibration_status(self):
        '''Read the calibration status of the sensors and return a 4 tuple with
        calibration status as follows:
          - System, 3=fully calibrated, 0=not calibrated
          - Gyroscope, 3=fully calibrated, 0=not calibrated
          - Accelerometer, 3=fully calibrated, 0=not calibrated
          - Magnetometer, 3=fully calibrated, 0=not calibrated
        '''
        
        # Return the calibration status register value.
        data = self.i2c.mem_read(1,self.BNO055_I2C_ADDRESS,self.BNO055_CALIB_STAT_ADDR)
        
        # Conv Byte string to unsign int
        cal_status=int.from_bytes(data,'little')

        # Gets 2 significant bits for status of object
        sys = (cal_status >> 6) & 0x03
        gyro = (cal_status >> 4) & 0x03
        accel = (cal_status >> 2) & 0x03
        mag = cal_status & 0x03
        
        # Return the results as a tuple of all 4 values.
        return (sys, gyro, accel, mag)
    
    def _read_vector(self, address, count=3):
        '''#Read count number of 16-bit signed values starting from the provided
        address. Returns a tuple of the values that were read.
        
        @param adresse The first register_adresse to start reading values
        @param count  An interger of the number of 16-bit values, that should be read'''
        
        # Init array with size of count
        result = [0]*count        
        
        # Read a sign 16-bit value
        for i in range(count):
            data = self.i2c.mem_read(2, self.BNO055_I2C_ADDRESS, address+i*2)
            result[i]=self.conv_to_sign_int(data)
        
        # Return values as array
        return result
    
    def read_euler(self):       
        '''Return the current absolute orientation as a tuple of yaw, roll,
        and pitch euler angles in degrees.
        '''
        # Read Euler-Angles by starting with first Euler_Angle_Register
        yaw, roll, pitch = self._read_vector(self.BNO055_EULER_ADDR)
        
        return (yaw/16.0, roll/16.0, pitch/16.0)
    
    def read_magnetometer(self):
        '''Return the current magnetometer reading as a tuple of X, Y, Z values
        in micro-Teslas.'''
        
        x, y, z = self._read_vector(self.BNO055_MAG_DATA_ADDR)
        return (x/16.0, y/16.0, z/16.0)

    def read_gyroscope(self):
        '''Return the current gyroscope (angular velocity) reading as a tuple of
        X, Y, Z values in degrees per second.'''
        
        x, y, z = self._read_vector(self.BNO055_GYRO_DATA_ADDR)
        return (x/900.0, y/900.0, z/900.0)

    def read_accelerometer(self):
        '''Return the current accelerometer reading as a tuple of X, Y, Z values
        in meters/second^2.'''
        
        x, y, z = self._read_vector(self.BNO055_ACCEL_DATA_ADDR)
        return (x/100.0, y/100.0, z/100.0)

    def read_linear_acceleration(self):
        '''Return the current linear acceleration (acceleration from movement,
        not from gravity) reading as a tuple of X, Y, Z values in meters/second^2.'''
    
        x, y, z = self._read_vector(self.BNO055_LINEAR_ACCEL_DATA_ADDR)
        return (x/100.0, y/100.0, z/100.0)

    def read_gravity(self):
        '''Return the current gravity acceleration reading as a tuple of X, Y, Z
        values in meters/second^2.'''

        x, y, z = self._read_vector(self.BNO055_GRAVITY_DATA_ADDR)
        return (x/100.0, y/100.0, z/100.0)
    
if __name__ == '__main__':
    ''' The main function creates a IMU_Driver for the Bosch Sensor.
    The While_Loop prints the latest Euler Angles as well as the Calibration Status every 5 Seconds'''
    
    # Import Libarys
    from pyb import I2C
    import time
    
    # I2C address
    BNO055_I2C_ADDRESS = 0x28   
    
    #Operation Mode of IMU 
    Operation_Mode='NDOF'
    
    # Creates IMU_class
    imc=IMU_driver(BNO055_I2C_ADDRESS,Operation_Mode)
    
    a=0
    
    while 1:
        
        # Get Calibration Status and print it every 5 seconds
        if ( (a%5) == 0 ):
            cal_stat=imc.get_calibration_status()
            print('Calibration Status: \nSystem({:}), Gyro({:}), Acc({:}), Mag({:})'.format(cal_stat[0],cal_stat[1],cal_stat[2],cal_stat[3]))
    
        # Get Euler angles and print them
        euler_angles=imc.read_euler()
        print('Euler Angles: \nYaw({:}°), Pitch({:}°), Heading({:}°)'.format(euler_angles[0],euler_angles[1],euler_angles[2]))
        
        # Get linear acc and print them
        #lin_acc=imc.read_linear_acceleration()
        #print('Linear Acc: \nX({:}m/s²), Y({:}m/s²), Z({:}m/s²)'.format(lin_acc[0],lin_acc[1],lin_acc[2]))
        
        
        # Sleep for 1s
        time.sleep(1)
        
        a+= 1
                
    