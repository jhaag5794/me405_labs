# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 11:30:59 2020

@author: jenshaag
"""


''' @file main.py
There must be a docstring at the beginning of a Python source file
with an @file [filename] tag in it! '''

def fib (idx):
    ''' This method calculates a Fibonacci number corresponding to
    a specified index.
    @param idx An integer specifying the index of the desired
    Fibonacci number.''' 
    
    print ('Calculating Fibonacci number at '
           'index n = {:}.'.format(idx))
    fib1=0
    fib2=1
    n=1
    
    while idx>n:
        fib3=fib1+fib2
        fib1=fib2
        fib2=fib3
        n=n+1
    
    if idx == 0:
        fib3=fib1
    elif idx == 1:
        fib3=fib2 
        
    print ('The Fibonacci number is: {:d}'.format (fib3))

if __name__ == '__main__':
    
    contr_2='0'

    while contr_2 != '1':
        
        contr_1=1

        while contr_1:
            try:
               idx = int(input('Please enter an index for the desired Fibonacci number: '))       
            except ValueError:
               print("Not an integer! Try again.")
               continue
            else:
               contr_1=0 
              
        fib(idx)
        
        contr_2=input('For Exit press 1 ')