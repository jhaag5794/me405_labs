# -*- coding: utf-8 -*-

''' @file Motor_Controller.py
This file contains the class controller with all the commands for controlling the system

    
@author Jens Haag
@date 05/14/20'''


class controller:
    '''Controller object
    
    This class implements the controller 
    @author Jens Haag
    @date 05/13/20'''
    
    def __init__ (self,set_position,kp_gain,encoder_class,driver_class):
        '''Creates a Controller function by initializing Encoder_object, 
        Driver_object and initialize necessary variables for object.
        
        @param set_position An integer object of the target position
        @param kp_gain A float object of the Kp_Value of the controller
        @param encoder_class Contains the Encoder of the systems
        @param driver_class Contains the Driver_object of the systems'''
        
        # Store values for function
        self.set_position=set_position
        self.kp_gain=kp_gain
        
        # Init necessary variables for the use of the controller
        self.act_position=0
        self.err_sig=0
        self.act_sig=0
        
        # Helpful Output for Init
        print ('Init Controller, target_position={:} ; Kp_Value= {:}'.format(self.set_position,self.kp_gain))
        
        
    def change_setposition(self,set_position):
        '''Changes the target position of the system.
        Stores the new target position of the control
        
        @param set_position An integer object of the target position'''
        
        # Stores new target Position
        self.set_position=set_position
        
    def change_kp_gain(self,kp_gain):
        '''Changes the Kp Value of the system
        Stores the new kp value of the control
        
        @param kp_gain A float object of the Kp_Value of the controller'''        
        # Stores new target Position
        
        self.kp_gain=kp_gain
        
    def update(self,act_position):
        '''Controller Unit of the object.
        Gets latest Position of the Encoder and calculates the duty_cycle for controll
        
        @param act_position A integer object Latest Position of the Encoder'''
        
        # Calculates the Error-Signal
        self.err_sig=self.set_position-act_position
        
        # Calculates the Duty_Cycle
        self.act_sig=self.err_sig*self.kp_gain*(-1)
        
        # Checks if Duty_Cycle is out of Range
        if abs(self.act_sig)>100:
            if self.act_sig>100:
                self.act_sig=100
            else:
                self.act_sig=-100    
        
        # Returns calculated duty_cycle
        return self.act_sig

if __name__ == '__main__':
    pass



    
    
