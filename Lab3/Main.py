# -*- coding: utf-8 -*-
"""
Created on Thu May 13 11:48:53 2020

@author: jenshaag
"""


''' @file Main.py
This file contains is the function for do step responses with the motor'''

if __name__ == '__main__':
    
    # Import Functions
    from Motor_Encoder import Encoder
    from Motor_Controller import controller
    from Motor_Driver import MotorDriver
    
    # Import Libary
    import pyb
    import time
    
    target=1000
    
    length_ms=1000
    
    # Create a motor object passing in the pins and timer
    #moe = MotorDriver( EN_pin, IN1_pin , IN2_pin, timer_number, timer_freq)
    moe = MotorDriver('A10', 'B5', 'B4', 3, 20000)
    
    
    # Create a motor object passing in the pins and timer
    #enco = Encoder ( ENA_pin , ENB_pin , timer_number, prescaler , period) 
    enco = Encoder('B6','B7',4,0,0xFFFF)
    
    # Create a Controller object 
    contr= controller(target, 1,enco,moe)
    
    # Enable the motor driver
    moe.enable()  
    
    print(' ')
    
    # Create Var for Loop Control
    idx='0';
    
    # Create Var for Input and Output of Controller
    duty=0
    act_position=0
    
    while idx != '3':
        
        idx=input('Change Kp_Value (1), Change Setpoint_Value (2), End Programm (3) ')
        
        # Kp_gain with step response
        if (idx== '1'):
        
            # Create Var for Loop Control
            contr_1=1;
                    
            # Check valid duty_cycle input
            while contr_1:
                try:
                    kp_gain = float(input('Please enter the Kp_Value in [%/ticks]: '))       
                except ValueError:
                    print("Not an integer! Try again.")
                    continue
                else:
                    if (kp_gain<0):
                        print("Please enter a positiv Kp_Value")
                    else:
                        contr_1=0
            
            # Reset position of Encoder
            enco.reset_position()
            
            # Set new Kp_Value
            contr.change_kp_gain(kp_gain)

            # Init empty Arrays for Respons_Values
            position_var=[]
            time_var=[]
            
            # Init Var of Loop
            a=0
            
            # Store Starttime for delta_time
            start=time.ticks_ms()
            
            # Loop for Step_responce (2sec)
            while a<(length_ms/10):
                
                # Gets Motorposition from Encoder
                act_position=enco.get_updates()
                
                # Controller Unit; Calculates the duty_cycle 
                duty=contr.update(act_position)
                
                # Updates the duty_cycle of Motor
                moe.set_duty(duty)
                
                # Saves Position and time to Array
                position_var.append(act_position)
                time_var.append(time.ticks_ms())
                
                # Sleep for 10ms
                time.sleep(0.01)
                
                # Increase loop counter
                a=a+1
            
            
            print (' ')
            print ('Step Response (target={:} , Kp={:})'.format(target, kp_gain))
            print ('Time [ms], Position [Ticks]')
            print ('--------------')
            
            a=0
            # Plots all stored Values
            while a<(length_ms/10):
                diff=time_var[a]-start
                print ('{:},{:}'.format(diff, position_var[a]))
                a=a+1
            
            print ('--------------')
            print (' ')
        
        # Updates the setposition of the Controller
        elif (idx== '2'):
            
            # Create Var for Loop Control
            contr_1=1;
                    
            # Check valid setposition input
            while contr_1:
                try:
                    target = int(input('Please enter the target_position [ticks]: '))       
                except ValueError:
                    print("Not an integer! Try again.")
                    continue
                else:
                    contr_1=0
            
            # Updates setpoint of Controller
            contr.change_setposition(target)
            print (' ')
            
        else:
            print("Please enter an valid index! Try again.")
    
    # Turn of Motor at the end            
    moe.disable()
    print("Program ended")
